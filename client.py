#!/bin/env python3
import requests
import argparse
import json


class InfluxdbClient:
    def __init__(self, url='http://localhost:8086'):
        self._url = url + '/api/v2/'
        token        = "TOKEN"
        self._bucket = "my-bucket"
        self._org    = "my-org"
        self._setup_data    = {'username': 'user',
                               'password': 'password',
                               'org'     : self._org,
                               'bucket'  : self._bucket,
                               'token'   : token}
        self.AUTH_HEADER = {'accept'        : 'application/json',
                            'Content-type'  : 'application/json',
                            'Authorization' : f"Token {token}"}
        self.WRITE_HEADER = {'accept'        : 'application/json',
                             'Content-type'  : 'text/plain; charset=utf-8',
                             'Authorization' : f"Token {token}"}
        self._write_url = f"{self._url}write?org={self._org}&bucket={self._bucket}"
    
    def post(self, command, data=None):
        r = requests.post(f"{self._url}{command}",
                          headers=self.AUTH_HEADER,
                          data=json.dumps(data))
        r.raise_for_status()
        return r.json()

    def get(self, command, data=None):
        fields = "&".join([f"{k}={v}" for k,v in data.items()]) if data is not None else ""
        r = requests.get(f"{self._url}{command}?{fields}",
                         headers=self.AUTH_HEADER)
        r.raise_for_status()
        return r.json()

    def delete(self, command, data=None):
        r = requests.delete(f"{self._url}{command}",
                            headers=self.AUTH_HEADER,
                            data=json.dumps(data))
        r.raise_for_status()
    
    def write(self, data):
        r = requests.post(self._write_url,
                          headers=self.WRITE_HEADER,
                          data=data)
        r.raise_for_status()

    def setup(self):
        print("Setup influxdb")
        self.post("setup", self._setup_data)

    def get_dashboards(self):
        return self.get("dashboards")['dashboards']
    
    def get_orgs(self):
        return self.get("orgs")['orgs']
    
    def get_stacks(self):
        orgid = self.get_orgs()[0]['id']
        return self.get("stacks", {'orgID': orgid})['stacks']

    def export_template(self, filename):
        print(f"Exporting template to {filename}")
        orgid = self.get_orgs()[0]['id']
        data = {'orgIDs': [{'orgID': orgid}]}
        data = self.post("templates/export", data)
        open(filename, "w+").write(json.dumps(data))
    
    def import_template(self, filename):
        print(f"Importing template from {filename}")
        stacks = self.get_stacks()
        orgid = self.get_orgs()[0]['id']
        filedata = json.load(open(filename))
        data = {'orgID': orgid, 'template': {'contents': filedata}}
        if len(stacks) > 0:
            data['stackID'] = stacks[0]['id']
        self.post("templates/apply", data)
    
    def list_dashboards(self):
        print(f"Current dashboards")
        for board in self.get_dashboards():
            print(f"  {board['name']}")
    
    def delete_dashboards(self):
        print(f"Deleting dashboards")
        for board in self.get_dashboards():
            print(f"  {board['name']} ({board['id']})")
            self.delete(f"dashboards/{board['id']}")
    
def main():
    parser = argparse.ArgumentParser(description='Communicate with a influxd server.')
    parser.add_argument('--setup',             action='store_true', help='Setup the influx server to default values')
    parser.add_argument('--list-dashboards',   action='store_true', help='List all dashboards')
    parser.add_argument('--delete-dashboards', action='store_true', help='Delete all dashboards')
    parser.add_argument('--export-template',   action='store',      help='Export template to file ')
    parser.add_argument('--import-template',   action='store',      help='Import template from file')
    parser.add_argument('--write',             action='store',      help='Write data to influx')
    
    args = parser.parse_args()
    client = InfluxdbClient()
    
    if args.setup:
        client.setup()
        
    if args.list_dashboards:
        client.list_dashboards()
        
    if args.delete_dashboards:
        client.delete_dashboards()
        
    if args.export_template:
        client.export_template(args.export_template)

    if args.import_template:
        client.import_template(args.import_template)
        
    if args.write:
        client.write(args.write)
        
if __name__ == "__main__":
    main()

    
