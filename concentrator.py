import time
from functools import partial
from math import sin
import socket
import toml
import sys

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

if len(sys.argv) < 2:
    print("Provide path to influxdb configs file")
    exit(1)

configs = toml.load(sys.argv[1])

for config in configs.values():
    if 'active' in config and config['active']:
        conf = config
        break

token = conf['token']
org   = conf['org']
url   = conf['url']
bucket = "testbucket"

print(url, org, bucket, token)

client = InfluxDBClient(url=url, token=token)

write_api = client.write_api(write_options=SYNCHRONOUS)

def register_execute(tid, mf, time):
    sequence = ["thread,id=%s execute=%s,minor=%s" % (tid, time, mf)]
    write_api.write(bucket, org, sequence)

def listen_udp_data():
    UDP_IP = "127.0.0.1"
    UDP_PORT = 5005

    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))
    
    while True:
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        tid,mf,t = data.decode().split()
        
        register_execute(tid, mf, t)
        

if __name__ == '__main__':
    
    listen_udp_data()
        
