import socket
import time
from math import sin
import random
import sys

name = "A"
if len(sys.argv) > 1:
    name = sys.argv[1]

UDP_IP = "127.0.0.1"
UDP_PORT = 5005

threads = [(name + '.A60', 1, 1000000),
           (name + '.B60', 1, 2000000),
           (name + '.C15', 4, 4000000),
           (name + '.D8',  8, 8000000)]

print("UDP target IP: %s" % UDP_IP)
print("UDP target port: %s" % UDP_PORT)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

# Generate some requests.
count = 0
while True:
    time.sleep(1/60.0)
    for tid, period, t in threads:
        if count % period != 0:
            continue
        ct = random.random()
        et = t + 10000 * ct + 100000 * sin(t/1000000*count/1000.0)
        if count % (10 * 64) == period:
            et += 2000000
        minor = count % 64
        MESSAGE = "%s %02d %d" % (tid, minor, et)
        sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
        #register_execute(tid, et)
    count += 1
    
