#
#
#
export PATH=$PWD/influxdb2-2.2.0-linux-amd64:$PATH
if [ $# -gt 0 ]
then
    SESSION_DIR=$1
    echo "Using provided session dir $SESSION_DIR"
else
    SESSION_DIR=/tmp/sessions/`date +%Y%m%d_%H%M%S`
    echo "Using new session dir $SESSION_DIR"
    mkdir -p $SESSION_DIR
    DO_INFLUX_SETUP=1
fi

ARGS="--reporting-disabled --bolt-path $SESSION_DIR/influxd.bolt --engine-path $SESSION_DIR/engine"

echo "Starting influxd"
influxd $ARGS&

INFLUXD_PID=$!

sleep 1

export INFLUX_CONFIGS_PATH=$SESSION_DIR/configs
if [ "$DO_INFLUX_SETUP" != "" ]
then
    echo "Wait for deamon to start and then run influx setup with new user and bucket"
    ./client.py --setup
    ./client.py --import-template template.json

fi

echo "Press enter to shutdown"
read
kill $INFLUXD_PID
echo "Bye"
